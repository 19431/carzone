package com.hackathon.carmanagement;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CarRepository extends MongoRepository<Car, String> {
	Car findBy_id(ObjectId _id);
}
