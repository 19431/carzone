/**
 * 
 */
package com.hackathon.carmanagement;

/**
 * @author ooni1
 *
 */
public enum Style {
	HYBRID, ELECTRIC, HATCHBACK, SEDAN, TRUCK, WAGON, VAN, CONVERTIBLE, COUPE
}