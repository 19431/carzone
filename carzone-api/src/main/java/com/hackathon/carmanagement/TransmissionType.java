/**
 * 
 */
package com.hackathon.carmanagement;

/**
 * @author ooni1
 *
 */
public enum TransmissionType {
	MANUAL, AUTOMATIC
}
