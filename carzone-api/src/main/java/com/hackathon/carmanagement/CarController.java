/**
 * 
 */
package com.hackathon.carmanagement;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/api/v1/cars")
public class CarController {
	
	@Autowired
	private CarRepository carCollection;

	@Value("${car.name}")
	private String carName;
	
	@GetMapping("/") 
	public List<Car> getAllCars() {
		List<Car> carList = new ArrayList<Car>();
		try {
			carList = carCollection.findAll();
		} catch(ConversionFailedException e) {
			e.printStackTrace();
		}
		return carList;
	}
	
	@GetMapping("/{id}")
	public Car getCarById(@PathVariable("id") ObjectId id) {
		Car yourCar = new Car();
		try {
			yourCar = carCollection.findBy_id(id);
		} catch (Exception e) {
			e.getMessage();
		}
		return yourCar;
	}	
	
	@PutMapping("/{id}")
	public void changeCarDetails( PathVariable("id") ObjectId id, @Valid @RequestBody Car toBeChanged) {
		try {
			toBeChanged.set_id(id);
			carCollection.save(toBeChanged);
		} catch (Exception e) {
			e.getCause().toString();
		}
	}
	
	/*TODO 
	 * get car by other than ID */
	
	
	@PostMapping("/")
	public Car addCar(@Valid @RequestBody Car newCar) {
		try {
			newCar.set_id(ObjectId.get());
			carCollection.save(newCar);
		} catch (Exception e) {
			System.out.println("Bad Juju");
		}
		return newCar;
	}
	
	
	@DeleteMapping("{id}")
	public void removeCar(@PathVariable ObjectId id) {
		carCollection.delete(carCollection.findBy_id(id));
	}
	
}