/**
 * 
 */
package com.hackathon.carmanagement;

/**
 * @author ooni1
 *
 */
public enum FuelType {
	GASOLINE, DIESEL, ELECTRIC, HYBRID
}
