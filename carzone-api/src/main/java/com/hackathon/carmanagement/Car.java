package com.hackathon.carmanagement;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.data.annotation.Id;

/**
 * @author ooni1
 *
 */
public class Car {
	
	@Id
	private ObjectId _id;
	private String make;
	private String model;
	private short modelYear;
	private String color;
	private FuelType fuelType;
	private short noOfEngineCylinders;
	private DriveType driveType;
	private String trim;
	private int mileage;
	private List<String> options;
	private Style style;
	
	public Car() {
		
	}
	
	public Car(ObjectId _id, String make, String model, short modelYear, String color, FuelType fuelType, short noOfEngineCylinders,
			DriveType driveType, String trim, int mileage, List<String> options, Style style) {
		this._id = _id;
		this.make = make;
		this.model = model;
		this.modelYear = modelYear;
		this.color = color;
		this.fuelType = fuelType;
		this.noOfEngineCylinders = noOfEngineCylinders;
		this.driveType = driveType;
		this.trim = trim;
		this.mileage = mileage;
		this.options = options;
		this.style = style;
	}

	public String get_id() throws ConversionFailedException {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) throws Exception {
		this._id = _id;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public short getModelYear() {
		return modelYear;
	}

	public void setModelYear(short modelYear) {
		this.modelYear = modelYear;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public FuelType getFuelType() {
		return fuelType;
	}

	public void setFuelType(FuelType fuelType) {
		this.fuelType = fuelType;
	}

	public short getNoOfEngineCylinders() {
		return noOfEngineCylinders;
	}

	public void setNoOfEngineCylinders(short noOfEngineCylinders) {
		this.noOfEngineCylinders = noOfEngineCylinders;
	}

	public DriveType getDriveType() {
		return driveType;
	}

	public void setDriveType(DriveType driveType) {
		this.driveType = driveType;
	}

	public String getTrim() {
		return trim;
	}

	public void setTrim(String trim) {
		this.trim = trim;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

	public List<String> getOptions() {
		return options;
	}

	public void setOptions(List<String> options) {
		this.options = options;
	}

	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}
		
}
