/**
 * 
 */
package com.hackathon.carmanagement;

/**
 * @author ooni1
 *
 */
public enum DriveType {
	AWD, FWD, RWD
}
